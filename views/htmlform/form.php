<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Form</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Form pada HTML</h4>
	<p class="p-content">Setelah sebelumnya kita belaja mengenai HTML dasar pada tutorial sebelumnya, kali ini kita akan lanjutkan tutorial HTML kita mengenai Form pada HTML.</p>

	<h4 class="sub-heading">Apa itu Form ?</h4>
	<p class="p-content">Form adalah elemen HTML yang digunakan untuk menerima bermacam-macam masukan (input) dari pengguna web</p>

	<h4 class="sub-heading">Bagimana membuat Form ?</h4>
	<p class="p-content">Untuk membuat form pada HTML kita butuh sebuah tag yang namanya <span class="p-bold">&lt;form&gt;</span>, nah tag <span class="p-bold">&lt;form&gt;</span> inilah yang dapat menerima semua inputan user, kritikan dari pengguna web, login, registrasi, dan sebagainya. <br>Form juga memiliki beberapa atribut tambahan yang sangat penting, yaitu <span class="p-bold">action dan method</span>, nah atribut inilah yang nantinya kita gunakan untuk mengirim data, menyimpan data dan sebagainya.</p>

	<p class="p-content">Tag Form digunakan untuk membungkus elemen-elemen input di dalamnya dan berfungsi mengirim data input ke server.</p>

	<p class="p-content">Di dalam form sendiri akan ada beberapa elemen yang di gunakan, tetapi kita akan membahas elemen-elemen tersebut pada tutorial selanjutnya. <br>Tutorial ini kita hanya akan membahas mengenai apa itu form dan bagaimana cara penggunaanya.</p>

	<h4 class="sub-heading">Contoh Form :</h4>
	<div class="contoh-dokumen">
		<p>
			<span class="properti">
				&lt;!DOCTYPE
				html&gt;<br>
				&lt;html
				lang="en"&gt;<br>
				&lt;head&gt;<br>
				&nbsp;&nbsp;&lt;meta charset="UTF-8"&gt;<br>
				&nbsp;&nbsp;&lt;title&gt;Belajar Form&lt;/title&gt;<br>
				&lt;/head&gt;<br>
				&lt;body&gt;<br>
			</span>
			<span class="nbsp properti">&lt;form&gt;</span><br>
				<span class="p-bold" style="margin-left: 60px;">...</span><br>
				<span class="p-bold" style="margin-left: 60px;">...</span><br>
				<span class="p-bold" style="margin-left: 60px;">...</span><br>
			<span class="nbsp properti">&lt;/form&gt;</span><br>
			</span>
			<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
			</span>
		</p>
	</div>

	<p class="p-content">Teman-teman ingat untuk membuat form kita perlu sebuah tag HTML yang namanya <span class="p-bold">&lt;form&gt;&lt;/form&gt;</span>, didalam tag inilah nantinya kita akan simpan beberapa elemen HTML untuk form. <br>Misalkan saya punya sebuah Form yang seperti berikut :</p>
	<div class="contoh-dokumen">
		<form action="" method="post">
			<div class="form-group">
              <label for="nama">Nama :</label>
              <input type="text" id="nama" class="form-control" placeholder="masukan nama" name="nama" required>
            </div>
            <div class="form-group">
              <label for="email">Email :</label>
              <input type="email" id="email" class="form-control" placeholder="masukan email" name="email" required>
            </div>
             <div class="form-group">
              <label for="telp">No Telepon :</label>
              <input type="tel" id="telp" class="form-control" placeholder="masukan no telp" name="notlp" required>
            </div>
            <div class="form-group">
              <label for="pesan">Pesan</label>
              <textarea rows="10" placeholder="masukan pesan" class="form-control" name="pesan" required></textarea>
            </div>
            <button type="submit" class="btn btn-info" name="submit">Kirim Pesan</button>
		</form>
	</div>

	<p class="p-content">Contoh form diatas merupakan contoh form yang sedikit kompleks, saya gunakan sedikit CSS agar tampilannya lebih cantik dan menarik.</p>

	<br>
	<a href="" class="btn btn-default btn-prev disabled">Sebelumnya</a>
	<a href="index.php?htmlform=elemenForm" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
