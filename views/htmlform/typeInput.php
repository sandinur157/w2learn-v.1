<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Type Input</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Type pada input</h4>
	<p class="p-content">Elemen HTML input memiliki banyak type, type sendiri merupakan atribut dari input tersebut.</p>

	<h4 class="sub-heading">Macam-macam type pada input :</h4>

	<h4 class="sub-heading">1. type : text</h4>
	<p class="p-content">Input type text digunakan untuk memasukan huruf, karakter, angka, dll. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="text"&gt;</span>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		Nama : <input type="text" value="Example"> <br><br>
		Kelas : <input type="text" value="...">
	</div>
	<h4 class="sub-heading">2. type : password</h4>
	<p class="p-content">Input type password digunakan untuk menyembuyikan masukan / inputan dari user, biasanya input type password itu menggunakan karakter (bulat-bulat hitam kecil). <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="password"&gt;</span>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		Password : <input type="password" value="Example"> <br>
	</div>
	<h4 class="sub-heading">3. type : checkbox</h4>
	<p class="p-content">Input type checkbox digunakan untuk menambahkan pilihan satu atau lebih dari satu pemilihan data. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Makanan kesukaan :<span class="properti">&lt;/h4&gt;</span>
		<span class="properti">&lt;input type="checkbox"&gt;</span> Nasi Goreng<br>
		<span class="properti">&lt;input type="checkbox"&gt;</span> Nasi Padang<br>
		<span class="properti">&lt;input type="checkbox"&gt;</span> Nasi Jamblang<br>
		<span class="properti">&lt;input type="checkbox"&gt;</span> Nasi Lengko<br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Makanan kesukaan :</h4>
		<input type="checkbox"> Nasi Goreng<br>
		<input type="checkbox"> Nasi Padang<br>
		<input type="checkbox"> Nasi Jamblang<br>
		<input type="checkbox"> Nasi Lengko<br>
	</div>
	<p class="p-content">Untuk type input lainnya, akan kita bahas pada tutorial selanjutnya.</p>
	<br>
	<a href="index.php?htmlform=elemenForm" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?htmlform=typeInputLanjut" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
