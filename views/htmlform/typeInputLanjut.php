<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Type Input Lanjut</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Type pada input</h4>
	<p class="p-content">Setelah sebelumnya kita membahas sebagian dari type pada input, tutorial ini kita akan melanjutkan atribut type pada input selanjutnya.</p>

	<h4 class="sub-heading">4. type : radio</h4>
	<p class="p-content">Input type radio digunakan untuk melakukan satu pemilihan diantara 2 atau lebih data. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Jenis Kelamin :<span class="properti">&lt;/h4&gt;</span>
		<span class="properti">&lt;input type="radio" name="jk"&gt;</span> Laki-laki<br>
		<span class="properti">&lt;input type="radio" name="jk"&gt;</span> Perempuan<br>
	</div>
	<p class="p-content">Hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Jenis Kelamin :</h4>
		<input type="radio" name="jk"> Laki-laki
		<input type="radio" name="jk"> Perempuan
	</div>
	<h4 class="sub-heading">5. type : list-box</h4>
	<p class="p-content"> Input type list-box digunakan untuk melakukan satu pemilihan diantara 2 atau lebih data menggunakan drop-down. <br> Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Daftar Jurusan :<span class="properti">&lt;/h4&gt;</span><br>
		<span class="properti">&lt;select&gt;</span><br>
			<span class="properti nbsp">&lt;option&gt;</span>-- Daftar Jurusan --<span class="properti">&lt;/option&gt;</span><br>
			<span class="properti nbsp">&lt;option value="RPL"&gt;</span>RPL<span class="properti">&lt;/option&gt;</span><br>
			<span class="properti nbsp">&lt;option value="TKJ"&gt;</span>TKJ<span class="properti">&lt;/option&gt;</span><br>
			<span class="properti nbsp">&lt;option value="TKR"&gt;</span>TKR<span class="properti">&lt;/option&gt;</span><br>
			<span class="properti nbsp">&lt;option value="TSM"&gt;</span>TSM<span class="properti">&lt;/option&gt;</span><br>
		<span class="properti">&lt;/select&gt;</span><br>
	</div>
	<p class="p-content">Hasilnya :</p>
	
	<div class="contoh-dokumen">
		<h4>Daftar Jurusan :</h4>
		<select>
			<option>-- Daftar Jurusan --</option>
			<option value="RPL">RPL</option>
			<option value="TKJ">TKJ</option>
			<option value="TKR">TKR</option>
			<option value="TSM">TSM</option>
		</select>
	</div>
	<h4 class="sub-heading">6. type : submit</h4>
	<p class="p-content">Input type submit merupakan atribut input pada versi HTML sebelumnya, pada HTML5 sudah tidak disarankan lagi menggunakan (input type submit). <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Submit<span class="properti">&lt;/h4&gt;</span><br>
		<span class="properti">&lt;input type="submit" value="Example"></span>
	</div>
	<div class="contoh-dokumen">
		<h4>Submit</h4>
		<input type="submit" value="Simpan">
	</div>
	<p class="p-content">Untuk type submit sendiri kita harus menambahkan atribut yang namanya <span class="p-bold">value</span>.</p>

	<p class="p-content">Ada beberapa type lagi yang dimiliki oleh input, tetatapi kami tidak akan membahas semuanya, karena type input tersebut merupakan type input dari HTML5, untuk tutorial HTML5 kami akan pisah dengan tutorial HTML5 khusus.</p>

	<br>
	<a href="index.php?htmlform=atributInput" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="" class="btn btn-default btn-next disabled">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->

