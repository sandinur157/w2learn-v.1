<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 Keunggulan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 
	
	<h4 class="sub-heading">Keunggulan HTML5</h4>
	<p class="p-content">Pengembangan yang kita lakukan tidak perlu selalu mendownload versi terbaru dari HTML hanya perlu mengikuti apa saja Tag html baru yang diliris oleh W3C.<br><br></p>

	<p class="p-content">Tidak perlu biaya apapun untuk menggunakan HTML5. <br><br></p>

	<p class="p-content">Karena HTML5 bersifat open source, maka pengembangannya akan terus bertambah karena banyak developer hebat-hebat dunia terus ikut memberikan kontribusinya dan tanpa kita harus membayar mereka.<br><br></p>

	<p class="p-content">Bagi browser lama yang tidak mengerti HTML5 akan secara otomatis mengabaikan tag-tag HTML5 dan tetap menyediakan halaman website dengan  tidak menganggu/merusak penampilannya.<br><br></p>

	<p class="p-content">Kedepannya proses Embedding video kedalam website akan menjadi lebih gampang dan terstandarlisasi dengan HTML5, file video atau audio akan lebih gampang dipasang pada website dan dapat dinikmati pada platfom manapun seperti mobile, desktop, dan sebagainya.<br><br></p>
	

	<br>
	<a href="index.php?html5=struktur" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=halBaru" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->