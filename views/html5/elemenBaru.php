<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 Elemen Baru dan Fungsinya</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Elemen baru pada HTML5 dan fungsinya</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;article&gt;</td>	
			<td>Untuk konten eksternal, seperti Teks dan artikel-berita, blog, forum atau konten lain dari sumber eksternal</td>
		</tr>
		<tr>
			<td>&lt;aside&gt;</td>	
			<td>Untuk konten selain konten itu ditempatkan di samping konten yang masuk harus berkaitan dengan isi sekitar nya</td>
		</tr>
		<tr>
			<td>&lt;command&gt;</td>	
			<td>Sebuah tombol, atau radiobutton, atau sebuah kotak centang
		</td>
		</tr>
		<tr>
			<td>&lt;details&gt;</td>	
			<td>Untuk rician mengambarkan tentang sebuah dokumen atau bagian dari dokumen</td>
		</td>
		</tr>
		<tr>
			<td>&lt;summary&gt;</td> 
			<td>	Sebuah keterangan, atau ringkasan, dalam rincian elemen
		</td>
		</tr>
		<tr>
			<td>&lt;figure&gt;</td>	
			<td>Untuk pengelompokan bagian dari konten berdiri sendiri, bisa video juga</td>
		</td>
		</tr>
		<tr>
			<td>&lt;figcaption&gt;</td>	
			<td>Keterangan dari tokoh bagian</td>
		</td>
		</tr>
		<tr>
			<td>&lt;footer&gt;</td>	
			<td>Untuk footer dari dokumen atau bagian, dapat meliputi nama penulis, tanggal dokumen, informasi kontak atau informasi hak cipta</td>
		</td>
		</tr>
		<tr>
			<td>&lt;header&gt;</td>	
			<td>Untuk penerapan suatu dokumen atau bagian, dapat mencangkup navigasi</td>
		</td>
		</tr>
		<tr>
			<td>&lt;hgroup&gt;</td>	
			<td>Untuk bagian dari pos, menggunakan &lt;h1&gt; untuk &lt;h6&gt;, dimana yang terbesar adalah pos utama pada bagian tersebut, yang lain nya bisa sub-judul</td>
		</tr>
		<tr>
			<td>&lt;mark&gt;</td>	
			<td>Untuk teks yang harus disorot</td>
		</tr>
		<tr>
			<td>&lt;meter&gt;</td>	
			<td>Untuk pengukuran, digunakan hanya jika nilai nilai maksimum dan minimum diketahui</td>
		</tr>
		<tr>
			<td>&lt;nav&gt;</td>	
			<td>Untuk bagian navigasi atau menu</td>
		</tr>
		<tr>
			<td>&lt;progress&gt;</td>	
			<td>Keadaan pekerjaan berlangsung</td>
		</tr>
		<tr>
			<td>&lt;ruby&gt;</td>	
			<td>Untuk penjelasan ruby (catatan Cina atau Karakter)</td>
		</tr>
		<tr>
			<td>&lt;rt&gt;</td>	
			<td>Untuk penjelasan tentang penjelasan ruby</td>
		</tr>
		<tr>
			<td>&lt;rp&gt;</td>	
			<td>apa untuk menunjukan browser yang tidak mendukung elemen ruby</td>
		</tr>
		<tr>
			<td>&lt;section&gt;</td>	
			<td>Untuk bagian dalam dokumen, seperti Bab, Header, Footer, atau bagian lain dari dokumen</td>
		</tr>
		<tr>
			<td>&lt;time&gt;</td>	
			<td>Untuk menentukan waktu atau tanggal, atau keduanya</td>
		</tr>
	</table>

	<h4 class="sub-heading">Elemen Media</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;audio&gt;</td>	
			<td>Untuk konten multimedia, suara, musik atau streaming audio lainya</td>
		</tr>
		<tr>
			<td>&lt;video&gt;</td>	
			<td>Untuk konten video, seperti klip film atau Sreaming video lainya</td>
		</tr>
		<tr>
			<td>&lt;source&gt;</td>	
			<td>Untuk sumber suatu media pada elemen media, menjelaskan di dalam video atau media audio</td>
		</tr>
		<tr>
			<td>&lt;embed&gt;</td>	
			<td>Untuk mengisi embedded, contohnya seperti plug-in atau mengambil video dari youtube.</td>
		</tr>
	</table>

	<h4 class="sub-heading">Elemen Kanvas</h4>
	<p class="p-content">Elemen kanvas menggunakan Javascript untuk membuat gambar pada halaman web.</p>
	<table class="table tabel-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;canvas&gt;</td>
			<td>Untuk membuat gambar pada sebuah halaman web</td>
		</tr>
	</table>	
	 
	<h4 class="sub-heading">Elemen form</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;datalist&gt;</td>
			<td>Daftar pilihan untuk nilai input</td>
		</tr>
		<tr>
			<td>&lt;keygen&gt;</td>
			<td>Hasilkan kunci untuk mengotentikasi pengguna</td>
		</tr>
		<tr>
			<td>&lt;output&gt;</td>
			<td>Untuk berbagai jenis output, seperti output yang ditulis oleh script</td>
		</tr>
	</table>

	<p class="p-content" style="font-size: 12px; float: right;"><strong>Referensi :</strong><em> www.W3scholls.com</em></p>

	<div class="clear"></div>



	<br>
	<a href="index.php?html5=halBaru" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=typeInputBaru" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
