<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Type input baru pada HTML5 dan fungsinya</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Type input baru pada HTML5</h4>
	<p class="p-content">Setelah sebelumnya kita membahas mengenai elemen HTML5 beserta fungsinya, tutorial ini kita akan membahas mengenai type baru pada elemen input.</p>

	<h4 class="sub-heading">Type baru pada input</h4>
	<table class="table table-hover">
		<th>Atribut Input</th>
		<th>Penjelasan</th>
		<tr>
			<td>tel</td>
			<td>Nilai input untuk nomer handphone</td>
		</tr>
		<tr>
			<td>search</td>
			<td>Nilai input untuk pencarian pencarian</td>
		</tr>
		<tr>
			<td>url</td>
			<td>Nilai input untuk memasukan url</td>
		</tr>
		<tr>
			<td>email</td>
			<td>Untuk pengisian email atau lebih dari 1 email</td>
		</tr>
		<tr>
			<td>datetime</td>
			<td>Untuk pengisian waktu dan tanggal</td>
		</tr>
		<tr>
			<td>date</td>
			<td>Pengisian tanggal</td>
		</tr>
		<tr>
			<td>month</td>
			<td>Untuk pengisian bulan</td>
		</tr>
		<tr>
			<td>week</td>
			<td>penggisian dalam waktu minggu</td>
		</tr>
		<tr>
			<td>time</td>
			<td>Untuk pengisian waktu</td>
		</tr>
		<tr>
			<td>datetime-local</td>
			<td>Untuk pengisian lokal waktu dan tanggal</td>
		</tr>
		<tr>
			<td>number</td>
			<td>Untuk pengisian nomer</td>
		</tr>
		<tr>
			<td>hidden</td>
			<td>Untuk menyembunyikan elemen input.</td>
		</tr>
		<tr>
			<td>range</td>
			<td>Untuk pengisian rentang waktu</td>
		</tr>
		<tr>
			<td>color</td>
			<td>Untuk pengisian heksadesimal sebuah warna, contohnya #000 (hitam)</td>
		</tr>

	</table>
	<br><br>

	<p class="p-content" style="float: right; font-size: 12px;"><strong>Referensi :</strong> <em>www.w3scholls.com</em></p>
	<div class="clear"></div>

	<p class="p-content">type baru diatas merupakan atribut baru pada elemen input yang baru ada di HTML5, untuk cara penggunaan dan contohnya saya akan pisahkan ditutorial yang berbeda.</p>

	<br>
	<a href="index.php?html5=elemenBaru" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=typeInputBaruLanjutan1" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->