<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 penggunaan atribut baru pada input</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Penggunaan atribut input baru pada HTML5</h4>
	<p class="p-content">Tutorial ini merupakan tutorial terakhir yang membahas mengenai atribut baru pada HTML5, setelah sebelumnya kita mengetahui beberapa atribut input baru pada HTML5, tutorial ini kita masih akan melanjutkannya.</p>
	<h4 class="sub-heading">5. Placeholder</h4>
	<p class="p-content">Placeholder merupakan atribut baru pada input yang baru ada pada HTML5. <br> Placeholder Digunakan untuk mendeskripsikan singkat pada kolom input agar sesuai format yang diinginkan.</p>
	<p class="p-content">Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Placeholder
		<span class="properti">&lt;/h4&gt;</span><br>
		<span class="properti">&lt;input type ="text" placeholder="Masukan nama"&gt;</span><br>
	</div>

	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<h4>Placeholder</h4>
		<input type="text" placeholder="Masukan nama" class="form-control">
	</div>
	<p class="p-content">Dengan placeholder walaupun kita tidak memberikan nama pada input atau label, user akan tahu oh input ini untuk nama, karena kita sudah kasih placeholder, placeholder sendiri akan hilang pada saat kita mengetikan sesuatu di kolom input.</p>
	<h4 class="sub-heading">6. Required</h4>
	<p class="p-content">Yang terakhir ada yang namanya required, required digunakan untuk memaksa user untuk mengisi inputan yang sudah kita sediakan. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Required
		<span class="properti">&lt;/h4&gt;</span><br>
		<span class="properti">&lt;input type ="text" required=""&gt;</span><br>
	</div>
	<p class="p-content">Hasil :</p>
	<img src="public/img/required.jpg">
	
	<p class="p-content">Teman-teman juga harus tahu semua atribut yang sudah kita bahas baik pada tutorial ini maupun sebelumnya, atribut-atribut tersebut tidak akan jalan kalau kita tidak memasukannya kedalam form. <br> Begitu juga required, required akan muncul pada saat kita menekan tombol button. <br> beberapa atribut diatas merupakan atribut baru pada HTML5.</p>
	
	<br>
	<a href="index.php?html5=typeInputBaruLanjutan1" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=semantik" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->