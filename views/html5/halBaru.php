<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 Elemen Baru</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Elemen dan Atribut baru pada HTML5</h4>
	<p class="p-content">Karena HTML5 merupakan standar terbaru dari HTML yang dispesifikasikan oleh W3C World Wide Web Consortium banyak sekali fitur-fitur terbaru, elemen-elemen baru atribut yang baru, dan hal baru lainya.</p>

	<h4 class="sub-heading">Elemen baru</h4>
	<table class="table table-hover">
		<tr>
			<td>article</td>
			<td>aside</td>
			<td>audio</td>
		</tr>
		<tr>
			<td>canvas</td>
			<td>header</td>
			<td>mark</td>
		</tr>
		<tr>
			<td>command</td>
			<td>datagrid datalist</td>
			<td>template</td>
		</tr>
		<tr>
			<td>embed</td>
			<td>event-source</td>
			<td>figure</td>
		</tr>
		<tr>
			<td>footer</td>
			<td>nest</td>
			<td>output</td>
		</tr>
		<tr>
			<td>progress</td>
			<td>source</td>
			<td>time</td>
		</tr>
		<tr>
			<td>video</td>
			<td>meter</td>
			<td>nav</td>
		</tr>
	</table>

	<h4 class="sub-heading">Elemen form baru</h4>
	<table class="table">
		<tr>
			<td>email</td>
			<td>url</td>
			<td>tel</td>
		</tr>
		<tr>
			<td>search</td>
			<td>color</td>		
			<td>date</td>
		</tr>
		<tr>
			<td>range</td>
			<td>progress</td>
			<td></td>
		</tr>
	</table>


	<h4 class="sub-heading">Atribut input baru</h4>
	<table class="table table-hover">
		<tr>
			<td>autocomplite</td>
			<td>autofocus</td>
			<td>list</td>
		</tr>
		<tr>
			<td>min</td>
			<td>max</td>	
			<td>pattern</td>
		</tr>
		<tr>
			<td>placeholder</td>
			<td>required</td>
			<td>step</td>
		</tr>
	</table>

	<p class="p-content">Yang sudah saya sebutkan diatas merupakan hal-hal baru pada HTML5 dan masih banyak lagi yang belum saya sebutkan diatas, untuk fungsi-fungsinya sendiri akan kita bahas pada tutorial-tutorial selanjutnya.</p>

	<br>
	<a href="index.php?html5=keunggulan" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=elemenBaru" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
