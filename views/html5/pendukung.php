<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 Pendukung</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Pendukung HTML5</h4>
	<p class="p-content">HTML5 merupakan sesuatu hal yang baru yang masih sangat eksperimental oleh karena itu ada juga beberapa browser yang belum mendukung HTML5, tetapi kebanyakan web browser yang sudah modern itu mendukung adanya HTML5. <br></p>
	<h4 class="sub-heading">Apakah browser anda mendukung HTML5 ?</h4>
	<p class="p-content">Untuk sekarang-sekarang ini teman-teman jangan khawatir, teman-teman jangan khawatir kalau web browser yang dipakai oleh teman-teman itu todak di support oleh HTML5, hampir semua browser modern sudah mendukung fitur-fitur baru dari HTML5. <br><br>Tapi teman-teman harus hati-hati apabila ada dari teman-teman yang masih menggunakan IE9 kebawah itu. IE9 kebawah itu tidak mendukung fitur-fitur baru dari HTML5. <br><br></p>
	<p class="p-content">
Jika teman-teman ingin mengetahui seberapa baik browser yang digunakan teman-teman saat ini apakah mendukung HTML5 atau tidak. <br><br> Coba mampir ke <a href="http://www.html5test.com">HTML5test.com</a> Situs ini akan mengecek apakah browser anda mendukung pengkodean kalimat HTML5, dalam kanvas, fungsi drag-and-drop file, pelekatan audio dan video dan semua elemen lain yang dibutuhkan dalam spesifikasi HTML5, begitu pula spesifikasi yang terkait namun bukan bagian dari HTML 5, seperti geolocation dan penyimpanan lokal (local storage). <br><br> Begitu anda masuk situs itu dan menguji browser, anda akan diberi skor dengan ambang tertinggi 300 poin. Nilai itu mengindikasikan tingkat dukungan browser dalam fitur-fitur yang dibutuhkan dalam HTML5, termasuk poin bonus bagi browser yang menunjukkan hasil lebih canggih dari standar yang diminta.</p>

<p class="p-content">Apabila teman-teman ingin mencoba mengetes browser dari teman-teman, silahkan kunjungi link <a href="http://www.html5test.com">disini</a>.</p>
	<br>
	<a href="index.php?html5=pengenalan" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=struktur" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
