<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 Struktur</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Struktur pada HTML5</h4>
	<p class="p-content">Pada HTML5 struktur penulisan lebih sederhana lagi, walaupun pada tutorial-tutorial sebelumnya saya juga memakai strktur dari HTML5, karena menurut saya HTML5 itu cukup sederhana apa lagi jika teman-teman sudah mengenal simantik, untuk simantik sendiri kita akan bahas pada tutorial-tutorial selanjutnya.</p>

	<h4 class="sub-heading">Struktur dasar HTML5</h4>
	<div class="contoh-dokumen">
		<span class="properti">
	&lt;!DOCTYPE html&gt;<br>
	&lt;html lang="en"&gt;<br>
	&lt;head&gt;<br></span>
		<span class="properti nbsp">
		&lt;meta charset="UTF-8"&gt;</span><br>
		<span class="properti nbsp">
		&lt;title&gt;</span>Belajar HTML5<span class="properti">&lt;/title&gt;<br>
	&lt;/head&gt;<br>
	&lt;body&gt;<br></span>
		<span class="properti nbsp">&lt;p&gt;</span>Hello World<span class="properti">&lt;/p&gt;<br>
	&lt;/body&gt;<br>
	&lt;/html&gt;<br>
</span>
	</div>

	<p class="p-content">Abila kita lihat hasilanya, mungkin sekilas tidak ada perbedaan dengan versi HMTL sebelumnya, tapi sekarang browser tahu, oh website ini pakainya HTML5.</p>

	<div class="contoh-dokumen">
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<title>Belajar HTML5</title>
		</head>
		<body>
			<p>Hello World</p>
		</body>
		</html>
	</div>

	<p class="p-content">Pada versi-versi sebelumnya kita menuliskan doctype itu cukup panjang, contoh XHTML1.0 :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;!DOCTYPE html</span> PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"<span class="properti">&gt;</span>
	</div>
	
	<h4 class="sub-heading">Fungsi dari masing tag pada struktur HTML5 diatas :</h4>
	<ul id="ul-content">
		<li>Tag &lt;!DOCTYPE html&gt; adalah sebuah deklarasi atau digunakan untuk memberitahukan kepada browser halaman kita itu menggunakan HTML versi berapa, agar browser dapat menentukan bagaimana memperlakukan kode tersebut.</li>
		<li>Tag &lt;html&gt; merupakan tag pembuka dari dokumen HTML yang kita buat.</li>
		<li>Tag &lt;head&gt; digunakan untuk memberikan informasi tentang dokumen, didalam tag &lt;head&gt; juga kita bisa menambahkan tag- tag yang biasanya digunakan untuk memberikan informasi lain.</li>
		<li>Tag &lt;title&gt; digunakan untuk memberikan judul pada sebuah halaman.</li>
		<li>Tag &lt;body&gt;</li>
		<li>Tag &lt;p&gt; digunakan untuk menjadikan isi teks kita sebagai paragraf.</li>
	</ul>

	<br>
	<a href="index.php?html5=pendukung" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=keunggulan" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
