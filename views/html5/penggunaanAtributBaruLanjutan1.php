<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 penggunaan atribut baru pada input Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Penggunaan atribut input baru pada HTML5</h4>
	<p class="p-content">Pada tutorial sebelumnya kita sudah mengetahui beberapa atribut input baru pada HTML5, tutorial ini kita masih akan melanjutkan tutorial sebelumnya.</p>

	<h4 class="sub-heading">3. Minlength</h4>
	<p class="p-content">Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Min
		<span class="properti">&lt;/h4&gt;</span><br>
		<span class="properti">&lt;p&gt;</span>Masukan nama anda jangan kurang dai 3 karakter<span class="properti">&lt;/p&gt;</span><br>
		<span class="properti">&lt;input type ="text" minlength="3"&gt;</span><br>
	</div>
	<p class="p-content">Hasil :</p>
	<img src="public/img/min.jpg">
	<p class="p-content">Apabila anda memasukan karakter kurang dari 3 karakter maka akan muncul pesan</p>

	<h4 class="sub-heading">4. Maxlength</h4>
	<p class="p-content">Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Maxlenth
		<span class="properti">&lt;/h4&gt;</span><br>
		<span class="properti">&lt;p&gt;</span>Masukan nama anda jangan labih dari 8 karakter<span class="properti">&lt;/p&gt;</span><br>
		<span class="properti">&lt;input type ="text" maxlength="8"&gt;</span><br>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<h4>Maxlength</h4>
		<p>Masukan nama anda jangan dari kurang dari 8 karakter</p>
		<input type="text" maxlength="8" class="form-control">
	</div>
	<p class="p-content">Apabila anda memasukan karakter lebih dari 8 karakter maka inputannya tidak akan masuk. <br>Untuk atribut lainnya akan kita bahas pada tutorial selanjutanya.</p>

	<br>
	<a href="index.php?html5=penggunaanAtributBaru" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=penggunaanAtributBaruLanjutan2" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->