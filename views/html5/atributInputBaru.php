<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 penggunaan atribut baru pada input</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Penggunaan atribut input baru pada HTML5</h4>
	<p class="p-content">Setelah sebelumnya kita membahas mengenai type input baru pada HTML5, tutorial ini kita akan membahas mengenai atribut input baru pada HTML5.</p>

	<h4 class="sub-heading">Atribut input baru HTML5</h4>
	<table class="table table-hover">
		<tr>
			<td>autocomplite</td>
			<td>Digunakan untuk menghilangkan history dari textfield.</td>
		</tr>
		<tr>
			<td>autofocus</td>
			<td>Diguanakan agar pada saat membuka halaman textfield tersebut langsung terfokus (cursor sudah ada ditextfield).</td>
		</tr>
		<tr>
			<td>minlength</td>
			<td>Digunakan untuk menentukan batas minimal karakter yang bisa kita masukan di textfield.</td>
		</tr>
		<tr>
			<td>maxlength</td>
			<td>Digunakan untuk menentukan batas maksimal karakter yang bisa kita masukan di textfield.</td>
		</tr>
		<tr>
			<td>placeholder</td>
			<td>Digunakan untuk mendeskripsikan singkat pada kolom input agar sesuai format yang diinginkan.</td>
		</tr>
		<tr>
			<td>required</td>
			<td>Untuk mengaharuskan user memasukan input agar halaman dapat diproses.</td>
		</tr>
	</table>
	<br>
	<p class="p-content">Atribut diatas merupakan atribut yang paling sering digunakan, misalkan required digunakan untuk memvalidasi form input agar semuanya diisi, kemudian ada yang namanya autofocus pasti teman-teman sering lihatlah pada website-website diinternet yang pada saat memasukan kalian kehalaman login cursornya langsung terfokus, dan beberapa kegunaan lainya. <br>Untuk cara pengguanaanya kita akan bahas pada tutorial selanjutnya.</p>

	<br>
	<a href="index.php?html5=typeInputBaruLanjutan4" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=penggunaanAtributBaru" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->