<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>Type input baru pada HTML5 Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Type input baru pada HTML5 lanjutan</h4>
	<p class="p-content">Untuk type input pada HTML5 itu cukup banyak, mungkin saya tidak akan bahas semuanya melainkan beberapa saja yang nantinya akan sering kita gunakan.</p>

	<h4 class="sub-heading">1. type: search</h4>
	<p class="p-content"><strong>&lt;input type ="search"&gt;</strong>, tipe ini sangat mirip dengan input type-teks, kecuali yang khusus ditujukan untuk menangani istilah pencarian. <br><br>Contoh : </p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="search"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="search">
	</div>
	<p class="p-content">Secara visual itu tidak ada perbedaanya, tapi coba teman-teman ketikan sesuatu di textfiled tersebut nanti akan muncul tanda silang disebelah kanan.</p>

	<h4 class="sub-heading">2. type: url</h4>
	<p class="p-content"><strong>&lt;input type ="url"&gt;</strong> digunakan untuk membiarkan pengguna memasukkan dan mengedit URL. Nilai masukan secara otomatis divalidasi untuk memastikan URL itu kosong atau benar dengan format yang benar sebelum formulir dikirimkan. <br><br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="url"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="url" placeholder="www.w2learn.com">
	</div>
	<p class="p-content">type-url dengan type-text hampir sangat mirip, untuk merasakan perbedaanya teman-teman bisa coba sendiri.</p>

	<h4 class="sub-heading">3. type: email</h4>
	<p class="p-content"><strong>&lt;input type ="email"&gt;</strong> digunakan untuk membiarkan pengguna memasukkan dan mengedit alamat email, atau jika beberapa atribut ditentukan, daftar alamat email. <br><br> Nilai masukan secara otomatis divalidasi untuk memastikan alamat email kosong atau alamat email yang benar (atau daftar alamat) sebelum formulir dapat dikirim. <br><br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="email"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="email" placeholder="example@gmail.com">
	</div>
	<p class="p-content">type email sendiri bisa menvalidasi sendiri inputan dari user, biasanya apabila kita tidak menambahkan karakter @ biasanya akan muncul pesan peringatan, agar teman-teman lebih paham lagi teman-teman bisa coba sendiri.</p>

 	

	<h4 class="sub-heading">10. type: number</h4>
	<p class="p-content"><strong>&lt;input type ="number"&gt;</strong> digunakan untuk membiarkan pengguna memasukkan nomor. Ini termasuk validasi internal untuk menolak inputan non-numerik. <br><br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;input type="number"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<div class="contoh-dokumen">
		<input type="number">
	</div>
	<p class="p-content">Perhatian type number hanya diutarakan untuk number atau angka, jika teman-teman memaksa untuk memasukan di type number ini maka inputannya tidak akan masuk.</p>

	
	<p class="p-content" style="float: right; font-size: 12px;"><strong>Referensi :</strong> <em><a href="https://developer.mozilla.org" target="_blank">https://developer.mozilla.org</a></em></p>
	<div class="clear"></div>
	<br>
	<a href="index.php?html5=typeInputBaru" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=typeInputBaruLanjutan2" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->
