<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML5 penggunaan atribut baru pada input</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div> 

	<h4 class="sub-heading">Penggunaan atribut input baru pada HTML5</h4>
	<p class="p-content">Setelah kita mengetahui atribut baru pada element input yang sudah kita bahas pada tutorial sebelumnya, tetapi pada tutorial sebelumnya kita belum mencoba cara penggunaanya. <br><br>Tutorial kali ini kita akan membahas bagaimana cara menggunakan atribut input tadi, sebetulnya untuk cara penggunaan sendiri mungkin hampir-mirip dengan atribut input yang sudah kita bahas pada tutorial HTML, mungkin akan ada sedikit perbedaan kita akana ketahui pada tutorial kali ini.</p>
	<h4 class="sub-heading">1. Autocomplite</h4>
	<ul id="ul-content">
		<li>Valuenya ada 2 :</li>
		<li>on adalah nilai defaultnya, pada saat kita tidak menambahkan atribut autocomplite itu defaultnya dalah on.</li>
		<li>off digunakan untuk mematikan history dari inputan kita,</li>
	</ul>
	<p class="p-content">Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>autocomplite = on
		<span class="properti">&lt;/h4&gt;</span><br>
		Nama <br>
		<span class="properti">&lt;input type ="text" autocomplite="on"&gt;</span><br>

		<span class="properti">&lt;h4&gt;</span>autocomplite = off
		<span class="properti">&lt;/h4&gt;</span><br>
		Nama <br>
		<span class="properti">&lt;input type ="text" autocomplite="off"&gt;</span>
	</div>
	<p class="p-content">Hasil :</p>
	<img src="public/img/autocomplite.jpg">
	<p class="p-content">autocomplite yang nilainya=on itu akan memunculkan history, history dari inputan yang pernah kita input. <br>Sedangkan autocomplite=off itu tidak akan memunculkan history tersebut.</p>


	<h4 class="sub-heading">2. Autofocus</h4>
	<p class="p-content">Untuk autofocus sendiri defaultnya itu tidak kita kasih value. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>autofocus
		<span class="properti">&lt;/h4&gt;</span><br>
		Nama <br>
		<span class="properti">&lt;input type ="text" autofocus=""&gt;</span><br>
	</div>
	<p class="p-content">Hasil :</p>
	<img src="public/img/autofocus.jpg">
	<p class="p-content" style="margin-top: -100px;">Dengan autofocus membuat user sedikit nyaman karena user tidak perlu memindahkan cursornya ke textfield tersebut. <br>Dengan autofocus maka cursornya akan otomatis di textfield yang dikasih atribut tersebut. <br>Untuk atribut baru lainnya akan kita bahas pada tutorial selanjutnya.</p>

	<br>
	<a href="index.php?html5=atributInputBaru" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html5=penggunaanAtributBaruLanjutan1" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
	
</article>
<!-- akhir content -->