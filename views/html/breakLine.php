<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Break Line</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>

	<h4>Break Line pada HTML</h4>
	<p class="p-content">Break line pada HTML merupakan cara kita agar penulisan kode HTML kita dapat turun kebawah tanpa menggunakan tag <span class="p-bold">&lt;p&gt;</span> <br><br>
	Break line atau <span class="p-bold">&lt;br&gt;</span> juga tag pendukung dari paragraf. <br>
	Break line atau mudahnya baris baru sebetulnya sudah berkali-kali kita coba pada tutorial-tutorial sebelumnya. <br><br>
	Pada tutorial ini kita tidak akan membahas banyak tetapi kita cuman mengingat-ingat fungsi dari break line yang sudah sering kita coba. <br>
	Untuk membuat baris baru kita perlu menggunakan tag HTML yang namanya <span class="p-bold">&lt;br&gt;</span>.
	Dengan tag HTML <span class="p-bold">&lt;br&gt;</span> juga kita dapat menambahkan spasi dibawah atau enter boleh banyak. <br></p>

	<h4 class="sub-heading">Misalkan contoh :</h4>
	<div class="contoh-dokumen">
		<span class="properti">
			&lt;!DOCTYPE
			html&gt;<br>
			&lt;html
			lang="en"&gt;<br>
			&lt;head&gt;<br>
			<span class="nbsp">&lt;meta charset="UTF-8"&gt;</span><br>
			<span class="nbsp">&lt;title&gt;</span></span>Belajar Break Line<span class="properti">&lt;/title&gt;<br>
			&lt;/head&gt;<br>
			&lt;body&gt;<br>
		</span>

		<span class="nbsp properti">&lt;p&gt;</span>Ini adalah paragraf 1<span class="properti">&lt;/p&gt;</span><br>
		<span class="nbsp properti">&lt;p&gt;</span>Ini adalah paragraf 2<span class="properti">&lt;/p&gt;</span><br>
		
		<span class="properti">
			&lt;/body&gt;<br>
			&lt;/html&gt;<br>
		</span>
	</div>
	
	<p class="p-content">Sama seperti contoh sebelumnya, pasti teman-teman juga sudah tahu outputnya seperti apa.</p>
	<div class="contoh-dokumen">
		<p>Ini adalah paragraf 1</p>
		<p>Ini adalah paragraf 2</p>
	</div>
	
	<p class="p-content">Sekarang bagaimana caranya misalkan saya ingin jarak antara paragraf 1 dengan paragraf 2 itu lebih jauh lagi.</p>

	<h4 class="sub-heading">Caranya bagaimana ?</h4>
	<p class="p-content">Diantara caranya kita bisa menggunakan tag HTML <span class="p-bold">&lt;br&gt;</span>. <br>Maka akan menjadi seperti ini :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;p&gt;</span>Ini adalah paragraf 1<span class="properti">&lt;/p&gt;</span><br>
		<span class="properti">&lt;br&gt;</span><br>
		<span class="properti">&lt;br&gt;</span><br>
		<span class="properti">&lt;br&gt;</span><br>
		<span class="properti">&lt;p&gt;</span>Ini adalah paragraf 2<span class="properti">&lt;/p&gt;</span><br>
	</div>

	<p class="p-content">Dengan kita menambahkan tag <span class="p-bold">&lt;br&gt;</span> setelah paragraf 1, maka jarak antara paragraf 1 dengan paragraf 2 semakin jauh, yang tadinya satu baris menjadi dua baris dan seterusnya tergantung banyaknya tag <span class="p-bold">&lt;br&gt;</span>yang kita berikan. <br>
	Jika kita lihat hasilnya, maka akan :</p>
	<div class="contoh-dokumen">
		<p>Ini adalah paragraf 1</p><br><br><br>
		<p>Ini adalah paragraf 2</p>
	</div>

	<p class="p-content">Kemudian ada satu tag lagi yang hampir mirip dengan tag <span class="p-bold">&lt;br&gt;</span> dan tag ini juga merupakan tag pendukung dari paragraf.</p>

	<h4 class="sub-heading">Tag HTML <span class="p-bold">&lt;hr&gt;</span></h4>

	<p class="p-content"><span class="p-bold">&lt;hr&gt;</span> sendiri digunakan untuk menambahkan garis horizontal.</p>

	<h4 class="sub-heading">Contoh :</h4>
	<div class="contoh-dokumen">
		<span class="properti">&lt;p&gt;</span>Ini adalah paragraf 1<span class="properti">&lt;/p&gt;</span><br>
		<span class="properti">&lt;hr&gt;</span><br>
		<span class="properti">&lt;p&gt;</span>Ini adalah paragraf 2<span class="properti">&lt;/p&gt;</span><br>
		<span class="properti">&lt;hr&gt;</span><br>
	</div>

	<p class="p-content">Pabila kita tampilkan hasil outputnya maka dibawah paragraf 1 dan paragraf 2 akan ada garis horizontal yang panjang defaultnya adalah selebar dari parent atau pembungkus dari paragrafnya, kebetulan yang membungkus paragraf adalah body, maka panjangnya akan penuh dari ujung kiri body hingga ujung kanan. <br> Hasilnya :</p>
	<div class="contoh-dokumen">
		<p>Ini adalah paragraf 1</p>
		<hr>
		<p>Ini adalah paragraf 2</p>
		<hr>
	</div>

	<p class="p-content">Itulah beberapa hal yang bisa dilakukan oleh <span class="p-bold">&lt;br&gt;</span> dan <span class="p-bold">&lt;hr&gt;</span>. <br>Contoh yang kami berikan merupakan contoh sederhana, akan lebih menarik lagi apabila kita sudah belajar CSS, dengan menghubungkan elemen HTML dengan CSS, tetepi mungkin itu akan kita bahas pada tutorial-tutorial selanjutnya.</p>

	<br>
	<a href="index.php?html=formating" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=javascript" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->