<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Daftar Tag Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<p class="p-content">Setelah sebelumnya kita membahas sebagian dari semua tag HTML, kali ini kita akan melanjutkan tutorial sebelumnya.</p>
	<h3 class="sub-heading">Daftar tag HTML lanjutan</h3>

	<h4 class="sub-heading">Tag Daftar</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;dd&gt;</td>
			<td>Menentukan definisi untuk istilah dalam daftar definisi.</td>
		</tr>
		<tr>
			<td>&lt;dir&gt;</td>
			<td>Mendefinisikan daftar direktori.</td>
		</tr>
		<tr>
			<td>&lt;dl&gt;</td>
			<td>Mendefinisikan daftar definisi.</td>
		</tr>
		<tr>
			<td>&lt;dt&gt;</td>
			<td>Mendefinisikan sebuah istilah (item) dalam daftar definisi.</td>
		</tr>
		<tr>
			<td>&lt;li&gt;</td>
			<td>Mendefinisikan daftar item.</td>
		</tr>
		<tr>
			<td>&lt;ol&gt;</td>
			<td>Mendefinisikan sebuah daftar yang dipesan.</td>
		</tr>
		<tr>
			<td>&lt;menu&gt;</td>
			<td>Merupakan daftar perintah.</td>
		</tr>
		<tr>
			<td>&lt;ul&gt;</td>
			<td>Mendefinisikan daftar unordered.</td>
		</tr>
		<tr>
			<td>&lt;dd&gt;</td>
			<td>Menentukan definisi untuk istilah dalam daftar definisi.</td>
		</tr>
		<tr>
			<td>&lt;dir&gt;</td>
			<td>Mendefinisikan daftar direktori.</td>
		</tr>
		<tr>
			<td>&lt;dl&gt;</td>
			<td>Mendefinisikan daftar definisi.</td>
		</tr>
		<tr>
			<td>&lt;dt&gt;</td>
			<td>Mendefinisikan sebuah istilah (item) dalam daftar definisi.</td>
		</tr>
		<tr>
			<td>&lt;li&gt;</td>
			<td>Mendefinisikan daftar item.</td>
		</tr>
		<tr>
			<td>&lt;ol&gt;</td>
			<td>Mendefinisikan sebuah daftar yang dipesan.</td>
		</tr>
		<tr>
			<td>&lt;menu&gt;</td>
			<td>Merupakan daftar perintah.</td>
		</tr>
		<tr>
			<td>&lt;ul&gt;</td>
			<td>Mendefinisikan daftar unordered.</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag Tabel</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;caption&gt;</td>
			<td>Mendefinisikan judul sebuah tabel.</td>
		</tr>
		<tr>
			<td>&lt;col&gt;</td>
			<td>Mendefinisikan nilai atribut untuk satu atau beberapa kolom dalam sebuah tabel.</td>
		</tr>
		<tr>
			<td>&lt;colgroup&gt;</td>
			<td>Menentukan atribut untuk beberapa kolom dalam sebuah tabel.</td>
		</tr>
		<tr>
			<td>&lt;table&gt;</td>
			<td>Mendefinisikan tabel data.</td>
		</tr>
		<tr>
			<td>&lt;tbody&gt;</td>
			<td>Kelompok satu set baris yang menentukan bagian utama data tabel.</td>
		</tr>
		<tr>
			<td>&lt;td&gt;</td>
			<td>Mendefinisikan sebuah sel dalam sebuah tabel.</td>
		</tr>
		<tr>
			<td>&lt;tfoot&gt;</td>
			<td>Kelompok satu set baris yang merangkum kolom tabel.</td>
		</tr>
		<tr>
			<td>&lt;thead&gt;</td>
			<td>Grup satu set baris yang menggambarkan label kolom dari sebuah tabel.</td>
		<tr>
			<td>&lt;th&gt;</td>
			<td>Mendefinisikan sebuah sel header dalam sebuah tabel.</td>
		</tr>
		<tr>
			<td>&lt;tr&gt;</td>
			<td>Mendefinisikan deretan sel dalam sebuah tabel.</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag Scripting</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;noscript&gt;</td>
			<td>Mendefinisikan konten alternatif untuk ditampilkan saat browser tidak mendukung scripting.</td>
		</tr>
		<tr>
			<td>&lt;script&gt;</td>
			<td>Tempat script dalam dokumen untuk pemrosesan sisi klien.</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag konten tertanam</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;applet&gt;</td>
			<td>Menyematkan applet Java (aplikasi mini Java) pada halaman.Gunakan &lt;object&gt;elemen sebagai gantinya.</td>
		</tr>
		<tr>
			<td>&lt;area&gt;</td>
			<td>Mendefinisikan area tertentu dalam peta gambar.</td>
		</tr>
		<tr>
			<td>&lt;audio&gt; </td>
			<td>Menyematkan suara, atau aliran audio dalam dokumen HTML.</td>
		</tr>
		<tr>
			<td>&lt;canvas&gt; </td>
			<td>Mendefinisikan sebuah wilayah dalam dokumen, yang bisa digunakan untuk menggambar grafis dengan cepat melalui scripting (biasanya JavaScript).</td>
		</tr>
		<tr>
			<td>&lt;embed&gt; </td>
			<td>Menyematkan aplikasi eksternal, biasanya konten multimedia seperti audio atau video menjadi dokumen HTML.</td>
		</tr>
		<tr>
			<td>&lt;figcaption&gt; </td>
			<td>Mendefinisikan sebuah caption atau legenda untuk sebuah gambar.</td>
		</tr>
		<tr>
			<td>&lt;figure&gt; </td>
			<td>Merupakan gambar yang digambarkan sebagai bagian dari dokumen.</td>
		</tr>
		<tr>
			<td>&lt;frame&gt;</td>
			<td>Mendefinisikan satu frame dalam frameset.</td>
		</tr>
		<tr>
			<td>&lt;frameset&gt;</td>
			<td>Mendefinisikan kumpulan frame atau frameset lainnya.</td>
		</tr>
		<tr>
			<td>&lt;iframe&gt;</td>
			<td>Menampilkan URL dalam bingkai sebaris.</td>
		</tr>
		<tr>
			<td>&lt;img&gt;</td>
			<td>Menampilkan gambar sebaris.</td>
		</tr>
		<tr>
			<td>&lt;map&gt;</td>
			<td>Mendefinisikan peta gambar sisi klien.</td>
		</tr>
		<tr>
			<td>&lt;noframes&gt;</td>
			<td>Mendefinisikan konten alternatif yang ditampilkan di browser yang tidak mendukung frame.</td>
		</tr>
		<tr>
			<td>&lt;object&gt;</td>
			<td>Mendefinisikan sebuah objek tertanam.</td>
		</tr>
		<tr>
			<td>&lt;param&gt;</td>
			<td>Mendefinisikan parameter elemen objek atau applet.</td>
		</tr>
		<tr>
			<td>&lt;source&gt; </td>
			<td>Mendefinisikan sumber media alternatif untuk elemen media seperti&lt;audio&gt;atau &lt;video&gt;.</td>
		</tr>
		<tr>
			<td>&lt;time&gt; </td>
			<td>Merupakan waktu dan / atau tanggal.</td>
		</tr>
		<tr>
			<td>&lt;video&gt; </td>
			<td>Menyematkan konten video dalam dokumen HTML.</td>
		</tr>
	</table>

	<p class="p-content">Tag-tag yang sudah kita bahas tersebut merupakan tag-tag yang ada pada HTML semenjak HTML versi awal hingga sekarang.</p>


	<br>
	<a href="index.php?html=tag" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=daftarAtribut"" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->