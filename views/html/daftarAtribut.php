<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML daftar Atribut</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Daftar atribut pada HMTL</h4>
	<p class="p-content">Pada tutorial-tutorial sebelumnya kita sudah pernah mebahas mengenai atribut pada HTML tetapi kita belum pelajari semua, yah pada tutorial ini kita akan membahas beberapa atribut pada HMTL yang belum kita bahas pada tutorial sebelumnya.</p>

	<h4 class="sub-heading">Tag Body</h4>
	<table class="table table-hover">
		<tr>
			<th>Atribut</th>
			<th>Value</th>
			<th>Fungsi</th>
		</tr>
		<tr>
			<td>bgcolor</td>
			<td>cth: blue, green, orange, dll.</td>
			<td>Untuk memberi warna latar belakang</td>
		</tr>
		<tr>
			<td>background</td>
			<td>cth: d/img.jpg, gambar.png, dll.</td>
			<td>Untuk menjadikan gambar sebagai latar belakang</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag p</h4>
	<table class="table table-hover">
		<tr>
			<th>Atribut</th>
			<th>Value</th>
			<th>Fungsi</th>
		</tr>
		<tr>
			<td>align</td>
			<td>cth: right, left, center, dll.</td>
			<td>Untuk menentukan perataan teks paragraf</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag hr</h4>
	<table class="table table-hover">
		<tr>
			<th>Atribut</th>
			<th>Value</th>
			<th>Fungsi</th>
		</tr>
		<tr>
			<td>size</td>
			<td>cth: 2, 10, 5, dll.</td>
			<td>Untuk menentukan ketebalan garis (dalam satuan px)</td>
		</tr>
		<tr>
			<td>color</td>
			<td>cth: reg, blue, green, dll.</td>
			<td>Untuk menentukan warna garis</td>
		</tr>
		<tr>
			<td>width</td>
			<td>cth: 100, 50%, dll.</td>
			<td>Untuk menentukan panjang garis</td>
		</tr>
	</table>
	
	<h4 class="sub-heading">Tag Form</h4>
	<table class="table table-hover">
		<tr>
			<th>Atribut</th>
			<th>Value</th>
			<th>Fungsi</th>
		</tr>
		<tr>
			<td>method</td>
			<td>cth: post dan get.</td>
			<td>Untuk menentukan method apa yang kita gunakan.</td>
		</tr>
		<tr>
			<td>action</td>
			<td>cth: index.php, d/index.html,  dll.</td>
			<td>Untuk menentukan aksi kita mau dikirim kemana.</td>
		</tr>
		<tr>
			<td>enctype</td>
			<td>cth: multipart/form-data.</td>
			<td>Digunakan pada saat kita bekerja dengan input type='file'</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag input</h4>
	<table class="table table-hover">
		<tr>
			<th>Atribut</th>
			<th>Value</th>
			<th>Fungsi</th>
		</tr>
		<tr>
			<td>size</td>
			<td>cth: 20, 50px, 100%, dll.</td>
			<td>Untuk mengatur panjang dari input / teks input.</td>
		</tr>
		<tr>
			<td>type</td>
			<td>cth: text, password, checkbox, dll.</td>
			<td>Untuk menentukan type input yang kita gunakan.</td>
		</tr>
		<tr>
			<td>name</td>
			<td>cth: nama, kelas, dll.</td>
			<td>Name digunakan pada saat kita bekerja dengan PHP.</td>
		</tr>
	</table>
	<h4 class="sub-heading">Tag option</h4>
	<table class="table table-hover">
		<tr>
			<th>Atribut</th>
			<th>Value</th>
			<th>Fungsi</th>
		</tr>
		<tr>
			<td>value</td>
			<td>cth: rpl, tkj, tkr, dll.</td>
			<td>Untuk menentukan isi dari opsi</td>
		</tr>
	</table>

	<h4 class="sub-heading">Dan lain-lain.</h4>
	<p class="p-content">Kita akan bahas daftar atribut lainya pada tutorial-tutorial berikutnya. <br>
	Kemudian ada atribut yang cukup penting, terutama pada saat kita menggunakan CSS dan JavaScript atribut ini akan sering digunakan untuk menyeleksi elemen HTML, ada 2 atribut yang akan sering kita gunakan pada saat kita belajar CSS dan JavaScript, yaitu :</p>
	<h4 class="sub-heading">1. id</h4>
	<h4 class="sub-heading">2. class</h4>
	<p class="p-content">Kita tidak akan bahas detail kedua atribut ini, melainkan kita hanya ketahui terlebih dahulu, karena ransanya untuk tutorial HTML ini kita jarang menggunakan 2 atribut tersebut, kemungkinan kita pakai 2 atribut tesebut untuk membuat link internal pada satu halaman mislakan pindah dari atas kebawah ataub kiri kekanan, itu kita bisa gunakan id untuk menjadi triger, untuk sementara kita abaikan dulu.</p>

	<br>
	<a href="index.php?html=tagLanjut" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="" class="btn btn-default btn-next disabled">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->