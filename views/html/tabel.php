<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Tabel</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Tabel pada HTML</h4>
	<p class="p-content">Tabel digunakan untuk menyajikan data dalam bentuk kolom dan baris, tujuannya agar informasi dapat ditampilkan secara lebih terstruktur dan tabular. <br><br>
	Tabel HTML dapat bermanfaat dalam kasus yang berbeda, di mana teman-teman dapat mengatur data seperti teks, daftar, link, gambar dalam tabel.</p>

	<h4 class="sub-heading">Contoh tabel :</h4>
	<div class="contoh-dokumen">
		<h2>Tabel Siswa</h2>
		<table class="table table-hover">
			<tr>
				<th>Nama</th>
				<th>Kelas</th>
				<th>Jurusan</th>
			</tr>
			<tr>
				<td>Denis</td>
				<td>XI</td>
				<td>Rekayasa Perangkat Lunak</td>
			</tr>
			<tr>
				<td>Budi</td>
				<td>XII</td>
				<td>Teknik Komputer Jaringan</td>
			</tr>
			<tr>
				<td>Desi</td>
				<td>X</td>
				<td>Perbankan Syariah</td>
			</tr>
		</table>
	</div>

	<p class="p-content">Tutorial ini kita tidak akan membuat tabel yang menggunakan CSS seperti diatas, melainkan kita hanya membuat tabel yang sederhana.</p>

	<div class="contoh-dokumen"><img src="public/img/tabel.jpg"></div>

	<h4 class="sub-heading">Bagaimana membuat tabel pada HTML ?</h4>
	<p class="p-content">Untuk membuat tabel pada HTML kita perlu sebuah tag HMTL yang namanya <span class="p-bold">&lt;table&gt;</span>. <br> Langsung saja kita buat sebuah tabel sederhana :</p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Tabel Siswa<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;table&gt;</span><br>
			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;th&gt;</span>Nama<span class="properti">&lt;/th&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;th&gt;</span>Kelas<span class="properti">&lt;/th&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;th&gt;</span>Jurusan<span class="properti">&lt;/th&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>Fathur<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>XII<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>Teknik Kendaraan Ringan<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>Jahid<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>XI<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>Teknik Speda Motor<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>
		<span class="properti">&lt;/table&gt;</span>
	</div>

	<p class="p-content">Keterangan :</p>
	<ul id="ul-content">
		<li>Tag <span class="p-bold">&lt;table&gt;</span> adalah tag yang digunakan untuk mendefinisikan sebuah tabel pada HTML.</li>
		<li>tag <span class="p-bold">&lt;tr&gt;</span> digunakan untuk mendefinisikan sebuah baris <span class="p-bold">&lt;tr&gt;</span> merupakan kependekan dari table row.</li>
		<li>tag <span class="p-bold">&lt;th&gt;</span> digunakan untuk membuat judul kolom (table heading).</li>
		<li>Tag <span class="p-bold">&lt;td&gt;</span> digunakan untuk mendefinisikan kolom tabel (table definition)</li>
	</ul>

	<p class="p-content">Untuk melihat hasilnya teman-teman bisa copy kode diatas.</p>

	<h4 class="sub-heading">Tambahkan border pada tabel</h4>
	<p class="p-content">Karena kita belum kasih border pada tabel nya, untuk menambahkan border pada tabel kita kita perlu atribut dari tabel yang namanya <span class="p-bold">border</span>.</p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;table border ="1"&gt;</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="properti">&lt;table&gt;</span>
	</div>

	<p class="p-content">Silahkan tambahkan atribut <span class="p-bold">border</span> seperti kode diatas.</p>

	<h4 class="sub-heading">Menambahkan padding antar sel</h4>
	<p class="p-content">Untuk menambahkan padding antar sel kita juga perlu atribut dari yang namanya <span class="p-bold">cellpadding</span>.</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;table border ="1" cellpadding="5"&gt;</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="properti">&lt;table&gt;</span>
	</div>

	<p class="p-content">Silahkan tambahkan atribut <span class="p-bold">cellpadding</span> seperti kode diatas.</p>

	<h4 class="sub-heading">Tambahkan spasi antar sel</h4>
	<p class="p-content">Untuk menambahkan spasi antar cel kita perlu atribut dari <span class="p-bold">table</span> yang namanya <span class="p-bold">cellspacing</span>.</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;table border ="1" cellpadding="5" cellspacing="5"&gt;</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="p-bold nbsp">...</span><br>
		<span class="properti">&lt;table&gt;</span>
	</div>

	<p class="p-content">Silahkan tambahkan atribut <span class="p-bold">cellspacing</span> seperti kode diatas. <br>Ada dua atribut lagi yang sering diguanakan dalam pembuatan tabel pada HTML, kita kan bahas dua atribut selanjutnya di tutorial selanjutnya.</p>
	
	<br>
	<a href="daftarLanjut" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=tabelLanjut" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->