<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Tabel Lanjutan</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Tabel pada HTML</h4>

	<h4 class="sub-heading">Atribut lain pada table :</h4>
	<img src="public/img/col-row.png">
	<ul id="ul-content">
		<li>Colspan untuk menggabungkan beberapa cell (column) dalam satu baris</li>
		<li>Rowspan untuk Menggabungkan beberapa cell (row) dalam satu kolom</li>
	</ul>

	<h4 class="sub-heading">Contoh Colspan :</h4>

	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Colspan<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;table border="1"  cellpadding="10" cellspacing="0"&gt;</span><br>
			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>1, 1<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>1, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>1, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 1<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 1<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>
		<span class="properti">&lt;/table&gt;</span>
	</div>

	<p class="p-content">Apabila kita tampilkan kurang lebih tampilannya akan seperti ini :</p>
	<div class="contoh-dokumen">
		<table border="1" cellpadding="10" cellspacing="0">
			<tr>
				<td>1, 1</td>
				<td>1, 2</td>
				<td>1, 3</td>
			</tr>

			<tr>
				<td>2, 1</td>
				<td>2, 2</td>
				<td>2, 3</td>
			</tr>

			<tr>
				<td>3, 1</td>
				<td>3, 2</td>
				<td>3, 3</td>
			</tr>
		</table>
	</div>

	<p class="p-content">Terus bagaimana caranya kalau kita ingin mengabungkan kotak 1,2 dengan 1,3, caranya dapat kita lakukan menggunakan colspan.</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Colspan<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;table border="1"  cellpadding="10" cellspacing="0"&gt;</span><br>
			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>1, 1<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td colspan="2"&gt;</span>colspan<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 1<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 1<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>
		<span class="properti">&lt;/table&gt;</span>
	</div>

	<p class="p-content">Jika kita jalankan di browser maka akan :</p>
	<div class="contoh-dokumen">
		<table border="1" cellpadding="10" cellspacing="0">
		<tr>
			<td>1, 1</td>
			<td colspan="2">colspan</td>
		</tr>

		<tr>
			<td>2, 1</td>
			<td>2, 2</td>
			<td>2, 3</td>
		</tr>

		<tr>
			<td>3, 1</td>
			<td>3, 2</td>
			<td>3, 3</td>
		</tr>
	</table>
	</div>

	<h4 class="sub-heading">Contoh rowspan :</h4>
	<p class="p-content">Bagaimana caranya kita mengabungkan antara baris 1, baris 2, dan baris 3, caranya kita bisa menggunakan rowspan.</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Colspan<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;table border="1"  cellpadding="10" cellspacing="0"&gt;</span><br>
			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td rowspan="3"&gt;</span>rowspan<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>1, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>1, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>2, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>

			<span class="properti nbsp">&lt;tr&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 2<span class="properti">&lt;/td&gt;</span> <br>
			<span class="properti" style="margin-left: 60px;">&lt;td&gt;</span>3, 3<span class="properti">&lt;/td&gt; </span><br>
			<span class="properti nbsp">&lt;/tr&gt;</span><br>
		<span class="properti">&lt;/table&gt;</span>
	</div>

	<p class="p-content">Kalau kode diatas kita tampilkan di browser, maka baris 1, baris 2, dan baris 3 akan digabungkan.</p>
	<div class="contoh-dokumen">
		<table border="1" cellpadding="10" cellspacing="0">
		<tr>
			<td rowspan="3">rowspan</td>
			<td>1, 2</td>
			<td>1, 3</td>
		</tr>

		<tr>
			<td>2, 2</td>
			<td>2, 3</td>
		</tr>

		<tr>
			<td>3, 2</td>
			<td>3, 3</td>
		</tr>
		
	</table>
	</div>

	<p class="p-content">Teman-teman juga bisa berkolaborasi lagi dengan menggunakan tabel, misalkan seperti berikut :</p>
	<div class="contoh-dokumen">
		<table border="1" cellpadding="10" cellspacing="0">
			<tr>
				<td>1, 1</td>
				<td>1, 2</td>
				<td>1, 3</td>
				<td>1, 4</td>
			</tr>

			<tr>
				<td>2, 1</td>
				<td colspan="2" rowspan="2"></td>
				<td>2, 4</td>
			</tr>

			<tr>
				<td>3, 1</td>
				<td>3, 4</td>
			</tr>

			<tr>
				<td>4, 1</td>
				<td>4, 2</td>
				<td>4, 3</td>
				<td>4, 4</td>
			</tr>
			
		</table>
	</div>
	<p class="p-content">Teman-teman juga harus ketahui, tabel yang kita buat merupakan contoh tabel yang cukup sederhana, nantinya setelah kita belajar CSS, desain tabel akan lebih cantik lagi.</p>

	
	<br>
	<a href="index.php?html=tabel" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=divSpan" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
