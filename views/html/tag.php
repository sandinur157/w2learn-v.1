<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML daftar Tag</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h3 class="sub-heading">berikut merupakan daftar tag pada HMTL</h3>
	<h4 class="sub-heading">Tag Struktural</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;!DOCTYPE&gt;</td>
			<td>Mendefinisikan tipe atau jenis dokumen</td>
		</tr>
		<tr>
			<td>&lt;a&gt;</td>
			<td>Mendefinisikan sebuah hyperlink.</td>
		</tr>
		<tr>
			<td>&lt;article&gt; </td>
			<td>Mendefinisikan sebuah artikel</td>
		</tr>
		<tr>
			<td>&lt;aside&gt; </td>
			<td>Mendefinisikan beberapa konten yang terkait secara longgar dengan konten halaman.</td>
		</tr>
		<tr>
			<td>&lt;body&gt;</td>
			<td>Mendefinisikan tubuh dokumen.</td>
		</tr>
		<tr>
			<td>&lt;br&gt;</td>
			<td>Menghasilkan jeda baris tunggal.</td>
		</tr>		
		<tr>
			<td>&lt;details&gt; </td>
			<td>Merupakan widget dimana pengguna dapat memperoleh informasi tambahan atau kontrol sesuai permintaan.</td>		
		</tr>
		<tr>
			<td>&lt;div&gt;</td>
			<td>Menentukan pembagian atau bagian dalam dokumen.</td>
		</tr>
		<tr>
			<td>&lt;h1&gt; to &lt;h6&gt;</td>
			<td>Mendefinisikan judul HTML.</td>
		</tr>
		<tr>
			<td>&lt;head&gt;</td>
			<td>Mendefinisikan bagian kepala dokumen yang berisi informasi tentang dokumen.</td>
		</tr>
		<tr>
			<td>&lt;header&gt; </td>
			<td>Merupakan header dokumen atau bagian.</td>
		</tr>
		<tr>
			<td>&lt;hgroup&gt; </td>
			<td>Mendefinisikan sekelompok judul.</td>
		</tr>
		<tr>
			<td>&lt;hr&gt;</td>
			<td>Menghasilkan garis horizontal.</td>
		</tr>
		<tr>
			<td>&lt;html&gt;</td>
			<td>Mendefinisikan akar dokumen HTML.</td>
		</tr>
		<tr>
			<td>&lt;footer&gt; </td>
			<td>Merupakan footer dari dokumen atau bagian.</td>
		</tr>
		<tr>
			<td>&lt;nav&gt; </td>
			<td>Mendefinisikan bagian dari link navigasi.</td>
		</tr>
		<tr>
			<td>&lt;p&gt;</td>
			<td>Mendefinisikan sebuah paragraf.</td>
		</tr>
		<tr>
			<td>&lt;section&gt; </td>
			<td>Mendefinisikan bagian dari dokumen, seperti header, footer dll.</td>
		</tr>
		<tr>
			<td>&lt;span&gt;</td>
			<td>Mendefinisikan sebuah bagian styleless inline dalam sebuah dokumen.</td>
		</tr>
		<tr>
			<td>&lt;summary&gt; </td>
			<td>Mendefinisikan ringkasan untuk details elemen.</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag Meta</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;base&gt;</td>
			<td>Mendefinisikan URL dasar untuk semua objek tertaut pada halaman.</td>
		</tr>
		<tr>
			<td>&lt;basefont&gt;</td>
			<td>Menentukan font dasar untuk halaman.</td>
		</tr>
		<tr>
			<td>&lt;link&gt;</td>
			<td>Mendefinisikan hubungan antara dokumen saat ini dan sumber eksternal.</td>
		</tr>
		<tr>
			<td>&lt;meta&gt;</td>
			<td>Menyediakan metadata terstruktur tentang konten dokumen.</td>
		</tr>
		<tr>
			<td>&lt;style&gt;</td>
			<td>Menyisipkan informasi gaya (biasanya CSS) ke dalam kepala dokumen.</td>
		</tr>
		<tr>
			<td>&lt;title&gt;</td>
			<td>Mendefinisikan judul untuk dokumen.</td>
		</tr>
	</table>

	<h4 class="sub-heading">Tag Pemformatan</h4>
	<table class="table table-hover">
		<tr>
			<th>Tag</th>
			<th>Penjelasan</th>
		</tr>
		<tr>
			<td>&lt;abbr&gt;</td>
			<td>Mendefinisikan bentuk kata atau frasa yang disingkat.</td>
		</tr>
		<tr>
			<td>&lt;acronym&gt;</td>
			<td>Mendefinisikan akronim.</td>
		</tr>
		<tr>
			<td>&lt;address&gt;</td>
			<td>Menentukan informasi kontak pengarang.</td>
		</tr>
		<tr>
			<td>&lt;b&gt;</td>
			<td>Menampilkan teks dengan gaya yang berani.</td>
		</tr>
		<tr>
			<td>&lt;bdi>&gt;</td>
			<td>Merupakan teks yang diisolasi dari sekitarnya untuk keperluan pemformatan teks dua arah.</td>
		<tr>
			<td>&lt;bdo&gt;</td>
			<td>Menimpa arah teks saat ini.</td>
		</tr>
		<tr>
			<td>&lt;big&gt;</td>
			<td>menampilkan teks dalam ukuran besar</td>
		</tr>
		<tr>
			<td>&lt;blockquote&gt;</td>
			<td>Mendefinisikan sebuah kutipan panjang.</td>
		</tr>
		<tr>
			<td>&lt;center&gt;</td>
			<td>Luruskan isi di tengah blok terlampir.</td>
		</tr>
		<tr>
			<td>&lt;cite&gt;</td>
			<td>Menunjukkan kutipan atau referensi ke sumber lain.</td>
		</tr>
		<tr>
			<td>&lt;code&gt;</td>
			<td>Menentukan teks sebagai kode komputer.</td>
		</tr>
		<tr>
			<td>&lt;del&gt;</td>
			<td>Menentukan blok teks yang dihapus.</td>
		</tr>
		<tr>
			<td>&lt;dfn&gt;</td>
			<td>Menentukan sebuah definisi.</td>
		</tr>
		<tr>
			<td>&lt;em&gt;</td>
			<td>Menentukan teks yang ditekankan.</td>
		</tr>
		<tr>
			<td>&lt;font&gt;</td>
			<td>Mendefinisikan font, warna, dan ukuran untuk teks.</td>
		</tr>
		<tr>
			<td>&lt;i&gt;</td>
			<td>Menampilkan teks dengan gaya miring.</td>
		</tr>
		<tr>
			<td>&lt;ins&gt;</td>
			<td>Mendefinisikan blok teks yang telah dimasukkan ke dalam </dokumen.</td>

		<tr>
			<td>&lt;kbd&gt;</td>
			<td>Menentukan teks sebagai input keyboard.</td>
		</tr>
		<tr>
			<td>&lt;mark>&gt;</td>
			<td>Merupakan teks yang disorot untuk tujuan referensi.</td>
		</tr>
		<tr>
			<td>&lt;output>&gt;</td>
			<td>Merupakan hasil perhitungan.</td>
		</tr>
		<tr>
			<td>&lt;pre&gt;</td>
			<td>Mendefinisikan blok teks yang telah diformat.</td>
		</tr>
		<tr>
			<td>&lt;progress>&gt;</td>
			<td>Merupakan kemajuan penyelesaian suatu tugas.</td>
		</tr>
		<tr>
			<td>&lt;q&gt;</td>
			<td>Mendefinisikan sebuah kutipan inline pendek.</td>
		</tr>
		<tr>
			<td>&lt;rp>&gt;</td>
			<td>Menyediakan tanda kurung mundur untuk browser yang tidak </mendukung anotasi ruby.</td>

		<tr>
			<td>&lt;rt>&gt;</td>
			<td>Mendefinisikan pengucapan karakter yang disajikan dalam </anotasi rubi.</td>

		<tr>
			<td>&lt;ruby>&gt;</td>
			<td>Merupakan anotasi ruby.</td>
		</tr>
		<tr>
			<td>&lt;s&gt;</td>
			<td>Menampilkan teks dengan gaya strikethrough.</td>
		</tr>
		<tr>
			<td>&lt;samp&gt;</td>
			<td>Menentukan teks sebagai contoh output dari program komputer.</</td>
		</tr>
		<tr>
			<td>&lt;small&gt;</td>
			<td>Menampilkan teks dalam ukuran yang lebih kecil.</td>
		</tr>
		<tr>
			<td>&lt;strike&gt;</td>
			<td>Menampilkan teks dengan gaya strikethrough.</td>
		</tr>
		<tr>
			<td>&lt;strong&gt;</td>
			<td>Tunjukkan teks yang sangat ditekankan.</td>
		</tr>
		<tr>
			<td>&lt;sub&gt;</td>
			<td>Mendefinisikan teks subscripted.</td>
		</tr>
		<tr>
			<td>&lt;sup&gt;</td>
			<td>Mendefinisikan teks superscripted.</td>
		</tr>
		<tr>
			<td>&lt;tt&gt;</td>
			<td>Menampilkan teks dengan gaya teletype.</td>
		</tr>
		<tr>
			<td>&lt;u&gt;</td>
			<td>Menampilkan teks dengan garis bawah.</td>
		</tr>
		<tr>
			<td>&lt;var&gt;</td>
			<td>Mendefinisikan sebuah variabel.</td>
		</tr>
		<tr>
			<td>&lt;wbr&gt;</td>
			<td>Merupakan peluang break line.</td>
		</tr>
	</table>

	<p class="p-content">Untuk daftar tag HTML lainya akan kami bahas pada tutorial selanjutanya.</p>


	<br>
	<a href="index.php?html=entitas" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=tagLanjut" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
