<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Hello World</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Kenapa kita harus menmpilkan Hello World di website kita ?</h4>
	<p class="p-content">Setelah sebelumnya kita mengetahui beberapa teks editor yang bisa digunakan untuk menuliskan kode HTML, tetapi kita belum mencoba untuk menggunakanya. <br>
	Oleh karena itu kita akan coba pada tutorial ini. <br><br>
	Karena sekarang kita mulai belajar bahasa yang baru, kita coba terlebih dahulu bagaimana menampilkan kata Hello World di halaman web pertama kita.
	<br><br>
	Untuk istalasi teks editor kita tidak akan bahas,  melainkan kita hanya bahas cara menggunakannya saja, kebetulan kita hanya menggunakan teks editor ini untuk menampilkan kata Hello World. <br><br>
	Teks editor yang akan saya gunakan pada tutorial ini adalah Sublime Text3, kenapa saya menggunakan Sublime Text3, seperti yang sayang utarakan pada tutorial sebelumnya. <br><br> Sublime Text3 merupakan aplikasi teks editor yang cukup hebat, dan menurut saya sangat banyak sekali keunggulananya dari pada teks editor lain, cuman kalau saya bahas semua keunggulanya pada tutorial ini mungkin akan cukup panjang. <br><br> Buat teman-teman yang ingin tahu keunggulan dari Sublime Text3 beserta cara penggunaanya, teman-teman bisa kunjungi website resminya di sublimetext3.
	</p>

	<h4 class="sub-heading">Langkah-langkah menampilkan Hello World</h4>

	<p class="p-content">1. Klik 2x aplikasi sublime text</p>
	<img class="hello" src="public/img/1.jpg"><br><br>

	<p class="p-content">2. Setelah aplikasi sublime text-nya aktif / sudah muncul</p>
	<img class="hello" src="public/img/2.jpg"><br><br>

	<p class="p-content">3. klik File > New File , atau tekan Ctrl+N untuk membuat file baru</p>
	<img class="hello" src="public/img/3.jpg"><br><br>

	<p class="p-content">4. Save File > pilih ekstensi file dengan .html</p>
	<img class="hello" src="public/img/4.jpg"><br><br>
	<p class="p-content">Atau</p>
	<img class="hello" src="public/img/5.jpg"><br><br>

	<p class="p-content">5. Setelah file di save tulis kata hello world di file kita.</p>
	<img class="hello" src="public/img/6.jpg">	<br><br>

	<p class="p-content">6. Setelah selesai tuliskan hello world nya teman-teman bisa klik kanan dibagian manapun dari sublime text3 kemudian open in browser, atau buka direktori tempat menyimpan file belajar1.html kemudian klik 2x, atau klik kanan open with google chrome.</p>
	<img class="hello" src="public/img/7.jpg"><br><br>
	
	<p class="p-content">Hello World sudah tampil, itu merupakan halaman web pertama kita, walaupun kita masih belum pakai struktur dari HTML sehingga browser tidak tahu kalau hello world merupakan halaman HTML, browser menganggap file halaman kita merupakan plain teks biasa. <br>Untuk sekarang tidak jadi masalah yang terpenting kita bisa menam[ilkan hello wrold terlebih dahulu di web browser. <br>Pada tutorial-tutorial selanjutnya kita akan pelajari mengenai struktur HTML begitu juga elemen-elemennya.</p>

	<br>
	<a href="index.php?html=editor" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=dasar" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->