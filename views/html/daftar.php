<!-- content --> 
<article class="boxbg" style="padding-bottom: 50px;">
	<h1>HTML Daftar</h1>
	<div class="iklan">
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
		  (adsbygoogle = window.adsbygoogle || []).push({
		    google_ad_client: "ca-pub-8107644676307326",
		    enable_page_level_ads: true
		  });
		</script>
	</div>
	
	<h4 class="sub-heading">Daftar pada HTML</h4>
	<p class="p-content">Daftar akan sering kita gunakan, misalkan pada saat kita ingin membuat Jadwal jam belajar, jadwal pelajaran, nama-nama siswa, dll.</p>

	<h4 class="sub-heading">Contoh List :</h4>
	<div class="contoh-dokumen">
		<div class="row">
			<div class="col-sm-4">
				<h4>jam belajar</h4>
				<ol>
					<li>07.00 - 12.00 Belajar disekolah</li>
					<li>13.00 - 14.00 Belajar dirumah</li>
					<li>18.00 - 19.00 Belajar dimasjid</li>
				</ol>
			</div>
			<div class="col-sm-4">
				<h4>Nama-nama siswa</h4>
				<ul>
					<li>Arifin</li>
					<li>Dinda</li>
					<li>Daniel</li>
					<li>Saip</li>
				</ul>
			</div>
			<div class="col-sm-4">
				<h4>Pemrograman Web</h4>
				<dl>
					<dt>HTML</dt>
					<dd>Hyper Text Markup Language</dd>
					<dt>CSS</dt>
					<dd>Cascading Style Sheet</dd>
					<dt>JS</dt>
					<dd>JavaScript</dd>
				</dl>
			</div>
		</div>
	</div>
	
	<h4 class="sub-heading">Macam-macam list :</h4>
	<ul id="ul-content">
		<li>Ordered List</li>
		<li>Unordered List</li>
		<li>Definition List</li>
	</ul>

	<h4 class="sub-heading">List - Ordered List</h4>
	<p class="p-content">Digunakan untuk membuat sebuah daftar dimana tiap bagiannya memiliki nomor secara terurut. <br>Contoh :</p>
	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>Urutan belajar web design<span class="properti">&lt;/h4&gt;</span><br>
		<span class="properti">&lt;ol&gt;</span> <br>
			<span class="properti nbsp">&lt;li&gt;</span>HTML (Heper Text Markup Language)<span class="properti">&lt;/li&gt;</span><br>
			<span class="properti nbsp">&lt;li&gt;</span>CSS (Cascading Style Sheet)<span class="properti">&lt;/li&gt;</span><br>
			<span class="properti nbsp">&lt;li&gt;</span>JavaScript<span class="properti">&lt;/li&gt;</span><br>
		<span class="properti">&lt;/ol&gt;</span>
	</div>

	<p class="p-content">Maka apabila ditampilkan hasilnya :</p>
	<div class="contoh-dokumen">
		<h4>Urutan belajar web design</h4>
		<ol>
			<li>HTML (Heper Text Markup Language)</li>
			<li>CSS (Cascading Style Sheet)</li>
			<li>JavaScript</li>
		</ol>
	</div>

	<p class="p-content">Defaultnya adalah angka 1 yang menjadi urutan dari ordered list ada beberapa type yang bisa kita berikan terhadap <span class="p-bold">&lt;ol&gt;</span> yaitu :</p>

	<ul id="ul-content">
		<li>1 (angka)</li>
		<li>A (A besar)</li>
		<li>a (a kecil)</li>
		<li>I (I besar)</li>
		<li>i (i kecil)</li>
	</ul>

	<h4 class="sub-heading">Cara menggunakanya bagaimana ?</h4>
	<p class="p-content">Untuk menggunakan type diatas kita butuh atribut dari <span class="p-bold">&lt;ol&gt;</span> yang namanya <span class="p-bold">&lt;type&gt;</span>. <br> Contoh :</p>

	<div class="contoh-dokumen">
		<span class="properti">&lt;h4&gt;</span>A besar<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;ol type="A"&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Ayah<span class="properti">&lt;/li&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Ibu<span class="properti">&lt;/li&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Anak<span class="properti">&lt;/li&gt; <br>
		&lt;/ol&gt;</span> <br>

		<span class="properti">&lt;h4&gt;</span>a kecil<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;ol type="a"&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Kyai<span class="properti">&lt;/li&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Ustadz<span class="properti">&lt;/li&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Santri<span class="properti">&lt;/li&gt; <br>
		&lt;/ol&gt;</span><br>

		<span class="properti">&lt;h4&gt;</span>I besar<span class="properti">&lt;/h4&gt;</span> <br>
		<span class="properti">&lt;ol type="I"&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Kepala Sekolah<span class="properti">&lt;/li&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Wali kelas<span class="properti">&lt;/li&gt;</span> <br>
		<span class="properti nbsp">&lt;li&gt;</span>Ketua Kelas<span class="properti">&lt;/li&gt; <br>
		&lt;/ol&gt;</span>
	</div>
	<div class="contoh-dokumen">
		<div class="row">
			<div class="col-sm-4">
				<h4>A besar</h4>
				<ol type="A">
					<li>Ayah</li>
					<li>Ibu</li>
					<li>Anak</li>
				</ol>
			</div>
			<div class="col-sm-4">
				<h4>a kecil</h4>
				<ol type="a">
					<li>Kyai</li>
					<li>Ustadz</li>
					<li>Santri</li>
				</ol>
			</div>
			<div class="col-sm-4">
				<h4>I besar</h4>
				<ol type="I">
					<li>Kepala Sekolah</li>
					<li>Wali kelas</li>
					<li>Ketua kelas</li>
				</ol>
			</div>
		</div>
	</div>

	<p class="p-content">Pada saat tampil di browser itu semua daftranya akan kebawah, kami mohon maaf kalau hasil output yang kami tampilkan ke samping karena memang sudah kami manipulasi dengan CSS. <br>Kami akan membahas daftar list yang lain pada tutorial selanjutnya, kami bagi menjadi beberapa bagian agar tutorialnya tidak terlalau panjang.</p>

	<br>
	<a href="index.php?html=gambar" class="btn btn-default btn-prev">Sebelumnya</a>
	<a href="index.php?html=daftarLanjut" class="btn btn-default btn-next">Selanjutnya</a>

	<div class="clear"></div>
</article>
<!-- akhir content -->
